To run this piece of software you need to have python3 environment.
File main.py is self-contained and uses one parameter which is the name
of csv file to be processed.

Example:

python3 main.py nameofcsv.csv